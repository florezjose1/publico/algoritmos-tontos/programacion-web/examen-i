// Create the chart
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Estadistica'
    },
    subtitle: {
        text: ''
    },
    accessibility: {
        announceNewData: {
            enabled: true
        }
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total numero de empleados'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> del Total<br/>'
    },

    series: [
        {
            name: "Empleados",
            colorByPoint: true,
            data: [
                {
                    name: "Bajo de Peso",
                    y: data1,
                },
                {
                    name: "Peso Normal",
                    y: data2,
                },
                {
                    name: "Sobre Peso",
                    y: data3,
                },
                {
                    name: "Obesidad",
                    y: data4,
                },
            ]
        }
    ],
})