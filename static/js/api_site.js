
// search form
document.getElementById('btn-open-form-search').addEventListener('click', function (){
    toggleFormSearch();
})
document.getElementById('btn-close-form-search').addEventListener('click', function (){
    toggleFormSearch();
})

function toggleFormSearch() {
    document.getElementById('form-search').classList.toggle('off');
}

// functions list auth
function actionAPI(data, method) {
    method = method.toLowerCase();
    return new Promise((resolve, reject) => {
        fetch(data.url, {
            method: method,
            body: JSON.stringify(data),
            headers: authHeader()
        }).then(response => {
            if ((method === 'post' && (response.status === 201 || response.status === 200)) ||
                (method === 'put' && response.status === 200)) {
                resolve(response.json().then(res => res))
            } else {
                reject(response);
            }
        }).catch((e) => {
            reject(e);
        })
    });
}

function getAPI(url, auth = false) {
    return new Promise((resolve, reject) => {
        fetch(url, {
            headers: auth ? authHeader() : ''
        }).then(response => {
            if (response.status === 200) {
                resolve(response.json().then(res => res))
            } else {
                reject(response);
            }
        }).catch((e) => {
            reject(e);
        })
    })
}

function updateAccess() {
    return new Promise((resolve, reject) => {
        try {
            let data = {
                "refresh": JSON.parse(localStorage.getItem('auth')).refresh
            }
            fetch('/api/user/refresh/', {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
                .then(response => {
                    if (response.status === 200) {
                        response.json().then(response => {
                            localStorage.setItem('auth', JSON.stringify(response));
                            resolve(response)
                        })
                    } else {
                        reject(response)
                    }
                }).catch(e => {
                reject(e)
            })
        } catch (e) {
            reject(e)
        }
    })

}
