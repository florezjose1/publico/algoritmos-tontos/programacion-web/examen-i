# Calculadora de masa corportal

Make sure Python 3.8 or higher is installed on your system.


###### Version django: `4.0.4`

### Description problem:
`previo1 A - 12022.pdf`


#### Clone Project
```
git clone 
```

#### Project setup
```
$ cd body_mass_index
$ virtualenv -p python3 env
$ source env/bin/activate
```

#### Install requirements
```
pip3 install --no-deps -r requirements.txt
```

#### Run 
```
python3 manage.py runserver
```

#### Mount DataBase
1. Run migrations
```
python3 manage.py migrate
```

#### Create super User
```
python3 manage.py cratesuperuser
```


Made with ♥ by [Jose Florez](www.joseflorez.co)
