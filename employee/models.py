from django.db import models


class Employee(models.Model):
    MANAGER = "manager"
    SUB_MANAGER = "sub_manager"
    TECHNICAL_LEADER = "technical_leader"
    PROFESSIONAL = "professional"
    OPERATOR = "operator"
    VARIUS = "various_services"
    POSITIONS_EMP = [
        (MANAGER, "Gerente"),
        (SUB_MANAGER, "Sub Gerente"),
        (TECHNICAL_LEADER, "Lider tecnico"),
        (PROFESSIONAL, "Profesional"),
        (OPERATOR, "Operador"),
        (VARIUS, "Servicios varios")
    ]
    name = models.CharField("Nombre empleado", max_length=100)
    position = models.CharField(
        max_length=20,
        choices=POSITIONS_EMP
    )
    years_old = models.IntegerField("Edad")
    weight = models.DecimalField("Peso (kg)", max_digits=10, decimal_places=2)
    height = models.DecimalField("Estadura", max_digits=10, decimal_places=2)
    body_mass_index = models.DecimalField("Indice de masa corporal", max_digits=10, decimal_places=2)

    class Meta:
        db_table = "employees"
