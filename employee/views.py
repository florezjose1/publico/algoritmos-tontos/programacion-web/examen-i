from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.decorators.http import require_http_methods
from django.urls import reverse_lazy
from employee.models import Employee


def render_index(request):
    return render(request, 'employee/index.html')


def render_statistics(request):
    """ 1) bajo de peso (IMC menor de 18.5), 2) peso
    normal (IMC entre 18.5 y 24.9), 3) sobrepeso (IMC entre 25
    y 29.9) y 4) en obesidad (IMC mayor o igual a 30)"""
    employees = Employee.objects.all()
    data1 = Employee.objects.filter(body_mass_index__lt=18.5).count()
    data2 = Employee.objects.filter(body_mass_index__gte=18.5, body_mass_index__lt=25).count()
    data3 = Employee.objects.filter(body_mass_index__gte=25, body_mass_index__lt=30).count()
    data4 = Employee.objects.filter(body_mass_index__gte=30).count()
    return render(request, 'employee/statistics.html', context={
        'employees': employees,
        'data1': data1,
        'data2': data2,
        'data3': data3,
        'data4': data4,
    })


def render_register(request):
    return render(request, 'employee/register.html')


@require_http_methods(['POST'])
def register_employee(request):
    """"""
    try:
        name = request.POST.get('name', None)
        years_old = request.POST.get('years_old', None)
        position = request.POST.get('position', None)
        weight = float(request.POST.get('weight', 0))
        height = float(request.POST.get('height', 0))
        imc = weight / height**2
        Employee.objects.create(
            name=name,
            years_old=years_old,
            position=position,
            weight=weight,
            height=height,
            body_mass_index=imc
        )
        return redirect(reverse_lazy('statics'))
    except Exception as e:
        messages.error(request, "Tenemos un error a la hora de registrar el empleado")
    return redirect(reverse_lazy('register'))
