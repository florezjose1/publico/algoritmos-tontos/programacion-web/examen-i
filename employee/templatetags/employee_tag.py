from django import template

register = template.Library()


@register.simple_tag
def get_classification(imc):
    """"""
    if imc < 18.5:
        return "Bajo de Peso"
    if imc < 25:
        return "Peso Normal"
    if imc < 30:
        return "Sobre Peso"

    return "Obesidad"
