from django.contrib import admin

from employee.models import Employee


@admin.register(Employee)
class SellerLiquidationReportAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "position",
        "years_old",
        "weight",
        "height",
        "body_mass_index",
    )

    list_filter = ("position",)

    readonly_fields = ("body_mass_index",)

    def save_model(self, request, obj, form, change):
        weight = obj.weight
        height = obj.height
        imc = weight / height ** 2
        obj.body_mass_index = imc
        obj.save()

